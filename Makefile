all:server client
server:server.o
	g++ -g -o server server.o
client:client.o
	g++ -g -o client client.o
server.o:server.cpp
	g++ -g -c server.cpp
client.o:client.cpp
	g++ -g -c client.cpp
clientp:client-ping.o
	g++ -g -o clientp client-ping.o
client-ping.o:client-ping.cpp
	g++ -g -c client-ping.cpp

serverp:server-ping.o
	g++ -g -o serverp server-ping.o
server-ping.o:server-ping.cpp 
	g++ -g -c server-ping.cpp 


clean:all
	rm all