#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>

#include <time.h>


#define MAXLINE 4096


// return time period between t1 and t2 (in milliseconds)
long int timeval_subtract(struct timeval *t2, struct timeval *t1)
{
    return (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
}

int main(int argc, char** argv){
    int   sockfd, n;
    char  recvline[4096], sendline[4096];
    struct sockaddr_in  servaddr;

    if( argc != 2){
        printf("usage: ./client <ipaddress>\n");
        return 0;
    }

    if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        printf("create socket error: %s(errno: %d)\n", strerror(errno),errno);
        return 0;
    }

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(6666);
    if( inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0){
        printf("inet_pton error for %s\n",argv[1]);
        return 0;
    }


    if( connect(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) < 0){
        printf("connect error: %s(errno: %d)\n",strerror(errno),errno);
        return 0;
    }



    int testing_count = 20;
    int rouni;
    for (rouni =0 ; rouni < testing_count ; rouni++)
    {

        // cgl ---- note the starting time
        struct timeval tvBegin, tvEnd, tvDiff;
        gettimeofday(&tvBegin, NULL);


        // printf("send msg to server: \n");
        // fgets(sendline, 4096, stdin);


        int strlens = 100;
        int i;
        for (i=0 ;i< strlens ; i++)
        {
            sendline[i] = 'A';
        }
        sendline[i] =  '\0';
        if( send(sockfd, sendline, strlen(sendline), 0) < 0){
            printf("send msg error: %s(errno: %d)\n", strerror(errno), errno);
            return 0;
        }

    struct timeval timeout={3,0};//3s
    int ret=setsockopt(sockfd,SOL_SOCKET,SO_SNDTIMEO,(const char*)&timeout,sizeof(timeout));
     ret=setsockopt(sockfd,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));

        n = recv(sockfd, recvline, MAXLINE, 0);
        if(n==-1&&errno==EAGAIN)
       {
            printf("[-]timeout\n");
            continue;
       }


        recvline[n] = '\0';
        printf("recv msg from server: %s\n", recvline);


        // note the ending time and calculate the duration of TCP ping
        gettimeofday(&tvEnd, NULL);
        long int diff = timeval_subtract(&tvEnd, &tvBegin);
        int secs = diff / 1000000;
        printf("  OK   Connected to %s:%d, seq=%d, time=%0.3lf ms\n", argv[1], servaddr.sin_port, rouni, diff/1000.);
        // sleeping until the beginning of the next second
        struct timespec ts;
        ts.tv_sec  = 0;
        ts.tv_nsec = 1000 * ( 1000000*(1+secs) - diff );
        nanosleep(&ts, &ts);



    }

        close(sockfd);



    return 0;
}
